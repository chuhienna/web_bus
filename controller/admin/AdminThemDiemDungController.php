<?php
require_once 'model/DiemDung.php';
$diemDungModel = new DiemDung();
require_once 'model/TuyenXe.php';
require_once 'model/TuyenBuyt.php';
$tuyenXeModel = new TuyenXe();
$tuyenBuytModel = new TuyenBuyt();
$result = $diemDungModel->view();
?>
<table id="user" class="table table-striped table-bordered" style="width:100%">
  <thead>
    <tr>
      <th>Mã Điểm dừng</th>
      <th>Điểm Dừng</th>
    </tr>
  </thead>
  <?php
      while($row = mysqli_fetch_assoc($result)){
  ?>
  <tbody>
    <tr>
      <td><?php echo $row['MaDiemDung']; ?></td>
      <td><?php echo $row['DiemDung']; ?></td>
    </tr>
  </tbody>
  <?php } ?>            
</table>
<h2>Nhập Mã Điểm Dừng để thêm </h2>
              <form method = "post">
                  <input type="number" name="MaDiemDung">
                  <br>
                  </br>
                      <button class="w- btn btn-lg btn-primary" type="submit" name="ThemDiemDung">Thêm</button>
                  <br>
              </form>
<?php
    if(isset($_POST['ThemDiemDung'])){
      $MaDiemDung = $_POST['MaDiemDung'];
      $MaTuyen = $_SESSION['MaTuyen'];
      $result = $tuyenBuytModel->add($MaTuyen, $MaDiemDung);
      if($result == true) {
        echo("<script>location.href = 'index.php?page=editBuses';</script>");
      echo "Thêm thành công";
      } else {
        echo "Error:";
      }
    }
?>